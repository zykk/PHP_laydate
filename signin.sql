CREATE TABLE `ns_member_signin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户ID',
  `starttime` int(11) NOT NULL COMMENT '签到开始日期',
  `lasttime` int(11) NOT NULL COMMENT '最后签到日期',
  `year` smallint(6) NOT NULL COMMENT '年份',
  `month` tinyint(3) NOT NULL COMMENT '月份',
  `total` int(11) NOT NULL DEFAULT '1' COMMENT '总签到天数',
  `xunum` int(11) NOT NULL DEFAULT '1' COMMENT '连续签到天数,最大30天',
  `signlist` text NOT NULL COMMENT '当月签到日期列表',
  `awardtotal` int(11) NOT NULL COMMENT '奖励总数',
  `awadesc` text NOT NULL COMMENT '奖励描述',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`,`starttime`,`lasttime`,`total`,`xunum`),
  KEY `year` (`year`,`month`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户签到数据';