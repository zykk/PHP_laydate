<?php
namespace app\wap\controller;

use data\service\Signin as SigninService;
/**
 * 
 * @author 
 *        
 */
class Signin extends SigninService
{
    public function __construct()
    {
        parent::__construct();
        $uid = $this->uid;
        if (empty($uid)) {
            $redirect = "/wap/login";
            $this->redirect($redirect); // 用户未登录
        }
    }
    // 签到页面
    public function  Signin(){
    	$signstatus  =  $this->getHasSign();
        // $signstatus = 1 ;
    	$this->assign('signstatus', $signstatus);
        return view('/Signin/Signin');    }
    // 签到状态
    public function getHasSign(){
    	$signservice  =  new SigninService();
    	$signlist  =  $signservice->hasSign($this->uid);
    	return $signlist;
    }
    // 获取签到日期范围
    public function getSignlist(){
    	$year  =  request()->post('year');
    	$month  =  request()->post('month');
    	$date  =  request()->post('date');
    	$signservice  =  new SigninService();
    	$signlist  =  $signservice->getMebSignInfos($year, $month, $this->uid);
    	return $signlist;
    }
    // 用户签到
    public function mebSignin(){
    	$signservice  =  new SigninService();
        $signstatus  =  $signservice->hasSign($this->uid);
        if ( $signstatus < 0 ) {
            return $signstatus;
        }
    	$signlist  =  $signservice->mebSignin($this->uid);
    	return $signlist;
    }

    
}


