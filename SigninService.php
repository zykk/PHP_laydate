<?php
namespace data\service;

use data\service\BaseService;
use data\model\MemberSigninModel;
use think\Model;

class SigninService extends BaseService
{
    // 获取用户签到数据
    public function getMebSignInfos( $year='', $month='', $uid='' )
    {
        if ( !$year || !$month || !$uid ) {
            return ['rescode'=>-2, 'msg'=>'日期数据有误！'];
        }

        $ckyear  =  cookie('ckyear');
        $ckmonth  =  cookie('ckmonth');
        
        $signlist  =  cookie('signlist');
        if ( !$signlist ) {
            cookie('ckyear', $year);
            cookie('ckmonth', $month);
            $signlist  =  $this->getlist($year, $month, $uid);
            return $signlist;
        }
        if ( $signlist && ( ($ckyear != $year && $ckmonth != $month) || ($ckyear == $year && $ckmonth != $month) ) ) {
            $signlist  =  $this->getlist($year, $month, $uid);
            
            return $signlist;
        }else{
            return $signlist;
        }

    }
    // 我的签到数据
    public function getlist($suyear, $sumonth, $uid){
        $membersign  =  new NsMemberSigninModel();
        $signwhere  =  ['year'=> $suyear, 'month'=>$sumonth, 'uid'=>$uid];
        $signinfo  =  $membersign->getInfo($signwhere);
        
        if ( $signinfo ) {
            $signinarray  =  explode(',', $signinfo['signlist']);
            $sarray  =  [];
            foreach ($signinarray as $key => $value) {
                $sarray[]  =  $signinfo['year'].'-'.$signinfo['month'].'-'.$value;
            }
            cookie('signlist', $sarray );
            return $sarray;
        }
        return ['rescode'=>-1, 'msg'=>'数据为空！'];
    }
    // 我的签到
    public function mebSignin($uid){
        $curyear  =  date("Y");
        $curmonth  =  date("n");
        $curday  =  date("j");
        $membersign  =  new NsMemberSigninModel();
        $signwhere  =  ['year'=> $curyear, 'month'=>$curmonth, 'uid'=>$uid];
        $signinfo  =  $membersign->getInfo($signwhere);
        $curtime  =  time();
        if ( !$signinfo ) {
            $signdata['uid'] = $uid;
            $signdata['starttime'] = $curtime;
            $signdata['lasttime'] = $curtime;
            $signdata['year'] = $curyear;
            $signdata['month'] = $curmonth;
            $signdata['signlist'] = $curday;
            $membersign->save($signdata);
        }else{
            $signdata['lasttime'] = $curtime;
            // $signdata['year'] = $signdata['year'];
            $signdata['awardtotal'] = $signdata['awardtotal'] + $this->getrewardata();
            $signdata['signlist'] = $signinfo['signlist'].','.$curday;
            $membersign->save($signdata, $signwhere);
        }
        $this->getlist($curyear, $curmonth, $uid);
        $this->getrewarinfo($uid);
        return 1;
        // return ['rescode'=>1, 'msg'=>'已签到'];
    }
    // 是否签到过
    public function  hasSign($uid){
        $curyear  =  date("Y");
        $curmonth  =  date("m");
        $curday  =  date("j");
        $membersign  =  new NsMemberSigninModel();
        $signwhere  =  ['year'=> $curyear, 'month'=>$curmonth, 'uid'=>$uid];
        $signinfo  =  $membersign->getInfo($signwhere);
        if ( !$signinfo ) {
            return 1;
        }
        $array  =  explode(',', $signinfo['signlist']);
        if ( in_array($curday, $array) ) {
            return -1;
        }
        return 1;

    }
    // 获取奖励数值
    public function getrewardata(){
        $score  =  2; 
        return $score;
    }
    // 赠送奖励
    public function getrewarinfo($uid){
        $score  =  $this->getrewardata();   // 默认奖励2
        // 记录 奖励数据，自行写
        // TODO ... 
    }
    
}